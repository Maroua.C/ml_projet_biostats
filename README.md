
### 1. Auteur
Maroua CHAHDIL  

### 2. But
Etude de donnes RNA-seq en machine learning

### 3. Librairies nécessaires :

Permet une construction de data frame, tableau necessaire pour l'execution du code.
Egalement contient des fonctions importante pour le machine learning

`pip install nympy`
`pip install panda`
`pip install sklearn`
`pip install pip install umap-learn`

tout cela est deja installer via sur l'environement conda disponible sur ce projet.

### 4. machinelearning.py :
Explication du fichier python contenant le code et comment l'executer

#### 4.1 Générer les données :

Telechargement des fichiers du **site** <https://archive.ics.uci.edu/ml/datasets/gene+expression+cancer+RNA-Seq> 
On obtient alors un fichier data.csv sous forme de tableau contenant tout les échantillons (20531) en X et en Y les 801 patients 
Puis un fichier labels.csv sous forme de tableau contenant les étiquettes (catégorie de cancer) 


Les données veront plus premier ligne et premier colonne supprimer pour une manipulation plus simple.



#### 4.2 Autre étape:

Une fois les données incrémenter dans un tableau panda

```
Y = y_label.iloc[:,0]
X = x_feature.iloc[:,0:]
```

Alors nous pouvons utiliser les fonctions de sklearn (ces fonctions peuvent aussi etre utilisé avec des tableaux de type numpy)

Nous avons fractionner les données.

Etape d'execution :
- Normalisation
- Réduction de dimension
    - LDA et PCA ne sont pas pertinent donc en commentaire cependant il est possible d'exécuter le modèle via ces approches cependnatil faudra mettre en commentraire la partie UMAP.
- Construction du model basé sur l'algorithme de regression liénaire
    - Régularisation
    - Validation croisée
    - Visualisation du model
    - Matrice de confusion
    - Learning rate

ATTENTION :
 Suite a cela a partir de la ligne environs 245 il y a le code pour exécuter notre modele avec l'algorithme d'arbres de décision.
 Si l'on veut executer l'arbre de decision alors il ne faut pas exécuter la partir regression liénaire sinon les données seront faussé.


 # Rapport 
Le rapport du projet est disponible [ici](https://docs.google.com/document/d/1Fd6najjR8uYg0A37Ex5JyKFWE-7usKHpiZ3YfX3hNTo/edit?usp=sharing)







